package Negocio;


/**
 * Write a description of class SopaDeLetras here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class SopaDeLetras
{
    // instance variables - replace the example below with your own
    private char sopas[][];

    /**
     * Constructor for objects of class SopaDeLetras
     */
    public SopaDeLetras()
    {
        
    }

    
 
    
    
    public SopaDeLetras(String palabras) throws Exception
    {
        if(palabras==null || palabras.isEmpty())
        {
            throw new Exception("Error no se puede crear la matriz de char para la sopa de letras");
        }
        
     //Crear la matriz con las correspondientes filas:
     
     String palabras2[]=palabras.split(",");
     this.sopas=new char[palabras2.length][];
     int i=0;
     for(String palabraX:palabras2)
     {
         //Creando las columnas de la fila i
         this.sopas[i]=new char[palabraX.length()];
         pasar(palabraX,this.sopas[i]);
         i++;
        
     }
     
     
    }
    
    private void pasar (String palabra, char fila[])
    {
    
        for(int j=0;j<palabra.length();j++)
        {
            fila[j]=palabra.charAt(j);
        }
    }
    
    
    public String toString()
    {
    String msg="";
    for(int i=0;i<this.sopas.length;i++)
    {
        for (int j=0;j<this.sopas[i].length;j++)
        {
            msg+=this.sopas[i][j]+"\t";
        }
        
        msg+="\n";
        
    }
    return msg;
    }
    
    
    public String toString2()
    {
    String msg="";
    for(char filas[]:this.sopas)
    {
        for (char dato :filas)
        {
            msg+=dato+"\t";
        }
        
        msg+="\n";
        
    }
    return msg;
    }
    
    
    
    public boolean esCuadrada()
    {
        boolean esCuadrada = true;           

        for(int i=0; i<this.sopas.length&&esCuadrada;i++){
            
            if(this.sopas[i].length != this.sopas.length)
            {
                esCuadrada = false;
            }            
            
        }
        
        return esCuadrada;
    }
    
    public boolean esDispersa()
    {
        boolean esDispersa = false;
        
        if(esCuadrada()==false&&esRectangular()==false)
        {
            esDispersa = true;
        }
        
        return esDispersa;
    }

    public boolean esRectangular()
    {
        boolean esRectangular = !esCuadrada();
        int numColumnas = sopas[0].length;
        for(int i = 0; i<sopas.length&&esRectangular;i++)
        {
            if(sopas[i].length != numColumnas)
            {
                esRectangular = false;
            }
        }
        return esRectangular;
    }
    
    /*
    retorna cuantas veces esta la palabra en la matriz
     */
    public int getContar(String palabra)
    {
        int contar = 0;
      buscarHorizontal(palabra);
        
       
        
        return contar;
    }
    
    public int buscarHorizontal(String palabra){
        int cantidad = 0;
        int k=0;
    
        for(int i=0; i<sopas.length; i++){
            for(int j=0; j<sopas[i].length; j++){
            
            if(sopas[i][j]==palabra.charAt(k)){
               k++;
            }else{
                  k=0;
                  if(sopas[i][j]==palabra.charAt(k))
                   {
                        k++;
                   }
            }
            
            if(k==palabra.length()){
             cantidad++;
             k=0;
            }
                
            }
        }
        
    
        return cantidad;
    }
    

    
    
    /*
    debe ser cuadrada sopas
     */
    public char []getDiagonalPrincipal() throws Exception
    {
        char [] diagonal = new char[sopas.length];
        
        boolean esCuadrada = esCuadrada();
        
        for(int i=0; i<sopas.length && esCuadrada == true;i++){
        
          diagonal[i] = sopas[i][i];
        }
        if(diagonal.equals(null)){
        
          throw new Exception();
          
        }
        return diagonal;
    }
    
    //Start GetterSetterExtension Source Code
    /**GET Method Propertie sopas*/
    public char[][] getSopas(){
        return this.sopas;
    }//end method getSopas

    //End GetterSetterExtension Source Code
//!
}
